Gem::Specification.new do |s|
  s.name          = 'muppet'
  s.version       = '0.0.1'
  s.date          = '2017-02-05'
  s.summary       = 'Muppet, a command line tool for working with puppet-devel'
  s.description   = 'Muppet, a command line tool for working with puppet-devel'
  s.authors       = ['Toni Schmidbauer']
  s.email         = 'toni@stderr.at'
  s.files         = ['bin/muppet']
  s.executables   = ['muppet']
  s.require_paths = ['lib/']
  s.homepage      = 'https://gitlab.com/nosolutions/muppet'
  s.license       = 'GPLv3'
  s.add_runtime_dependency(%q<gitlab>, ['>= 3.7.0', '< 4'])
  s.add_runtime_dependency(%q<jenkins_api_client>, ['>= 1.4.5', '< 2'])
  s.add_runtime_dependency(%q<thor>, ['>= 0.19.4', '< 0.20'])
  s.add_runtime_dependency(%q<tty>, ['>= 0.6.0', '< 0.20'])
end

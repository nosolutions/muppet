require 'muppet'
require 'muppet/config'



module Muppet
  module Gitlab
    class Config < Muppet::Config
      def initialize
        setting(:gitlab_host, 'gitlab.com', question: 'Enter the url to your gitlab host', desc: 'The gitlab host')
        setting(:gitlab_port, 443, question: 'Enter the port number for the gitlab web interface', desc: 'The gitlab web port')
        setting(:gitlab_ssh_port, 22, question: 'Enter the ssh port number of your gitlab instance ', desc: 'The gitlab ssh port')
        setting(:gitlab_groups, nil, question: 'Enter the gitlab groups you are using (comma separated) ', desc: 'Gitlab groups')
        setting(:gitlab_user, nil, question: 'Enter your gitlab username ', desc: 'Gitlab username')
      end
    end
  end
end

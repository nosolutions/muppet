require 'muppet/error'

module Muppet
  class Config
    def initialize
      @config = {}
      setting(:reaktor_uri, nil, question: 'Enter the url to your reaktor installation', desc: 'Reaktor URI')
      setting(:control_repo, nil, question: 'Enter the url to your control repository', desc: 'Control repo URI')
      setting(:site_repo, nil, question: 'Enter the url to your site repository', desc: 'Site repo URI')
      setting(:hiera_repo, nil, question: 'Enter the url to your hiera repository', desc: 'Hiera repo URI')
    end

    def method_missing(method_sym, *args, &block)
      attribute = method_sym.to_s
      if attribute[-1,1] == "="
        @config[attribute.chop][:value] = args[0]
        return
      else
        return @config[method_sym][:value] if @config.has_key? method_sym
      end
      raise Muppet::ConfigError, "Setting #{attribute} not found, try running `rake config`!"
    end

    def dump
      pp @settings
    end

    # def has_key?(key)
    #   return true if @config.has_key?(key.to_s)
    #   false
    # end

    def setting(var, default, question: nil, desc: nil)
      var_metadata = {
        :value => default,
        :question => question,
        :desc => desc,
      }
      @config[var] = var_metadata
    end

    def dump
      require 'pp'
      pp @config
    end

    private

    # remove any keys except :value
    def storable_config
      storable_config = {}
      @config.each do |k,v|
        storable_config[k] = {}
        storable_config[k][:value] = @config[k][:value]
      end
      storable_config
    end
  end
end

module Muppet
  class Error < StandardError
  end

  class ConfigError < Error
  end
end

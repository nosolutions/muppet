require 'yaml'

require 'muppet/questioner'
require 'muppet/config'
require 'muppet/gitlab/config'
require 'muppet/jenkins/config'

module Muppet
  class Configure
    CONFIG_FILE = File.join(File.expand_path('~'), '.muppet')

    def initialize
      @config = Muppet::Config.new
      @config.merge(Muppet::Gitlab::Config.new)
      @config.merge(Muppet::Jenkins::Config.new)
    end

    def interview
      @config.each_key do |k|
        @config[k][:value] = Muppet::Questioner.ask @config[k][:question], @config[k][:value]
      end
    end

    def save
      File.open(CONFIG_FILE, 'w') { |f| f.write YAML.dump(@config.storable_config) }
    end

    def load
      @config = YAML.load_file(CONFIG_FILE)
    end
  end
end

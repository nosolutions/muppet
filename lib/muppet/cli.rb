require 'thor'

require 'muppet/configure'

module Muppet
  class CLI < Thor

    desc "config", "configure muppet"
    long_desc <<-LONGDESC
    starts the interactive configuration dialog. You have to specify

    - Your name (Firstname Lastname)\n
    - Your e-mail address\n
    - The URL to your gitlab instance\n
    - The gitlab project you are using\n
    - Your gitlab username\n
    - Your gitlab password\n
    - The URL to your jenkins installation

    `muppet config` does not store your gitlab password. Instead it requests
    an API token from gitlab and uses this token in future requests.

    The configuration settings will be stored in ~/.muppet.yaml

    LONGDESC
    def config
      cfg = Muppet::Configure.new
      cfg.interview
      cfg.save
    end
  end
end

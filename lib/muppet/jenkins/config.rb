require 'muppet/config'

module Muppet
  module Jenkins
    class Config < Muppet::Config
      def initialize
        super
        setting(:jenkins_host, 'localhost', question: 'Enter the url to access your jenkins host', desc: 'Jenkins host')
        setting(:jenkins_port, 80, question: 'Enter the port number for the jenkins host', desc: 'Jenkins port')
        setting(:jenkins_user, nil, question: 'Enter your jenkins username', desc: 'Jenkins user')
      end
    end
  end
end

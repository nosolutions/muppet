require 'spec_helper'

require 'muppet/config'

describe Muppet::Config do

  describe '#setting' do
    settings = Muppet::Config.new

    settings.setting(:test, 'default', question: 'question', desc: 'desc')

    it 'config contains the test setting' do
      expect(settings.test).to eq('default')
    end
  end
end

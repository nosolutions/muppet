# Muppet

Muppet is a command line interface for [puppet-devel](https://gitlab.com/nosolutions/puppet-devel.git).

It allows you to

- Create new puppet modules
- Automatically create a corresponding gitlab project
- Automatically creates jenkins unittest jobs
- Let's you create releases for you puppet modules

  - updates metadata.json and
  - creates a corresponding git tag

- Let's you deploy puppet modules to specific environments
  - requires r10k
